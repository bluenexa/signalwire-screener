# build
FROM node:14-alpine AS build
WORKDIR /usr/src/app
COPY package*.json ./
COPY . .
RUN npm install
RUN npm run build

# run - public (no assets)
FROM node:14-alpine AS production
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci --only=production
COPY --from=build /usr/src/app/dist ./dist
CMD npm run start:production

# run - private (with contacts assets)
FROM production AS private
COPY assets ./assets
