#!/bin/bash

# build and push the docker images

VERSION=$(cat package.json | json version)

PUBLIC_IMAGE=registry.gitlab.com/bluenexa/signalwire-screener
PRIVATE_IMAGE=registry.gitlab.com/bluenexa/registry/signalwire-screener

if [[ "$1" != "--push" ]]; then
  echo building...
  docker build --rm --target production -t ${PUBLIC_IMAGE}:${VERSION}  -t ${PUBLIC_IMAGE}:latest .
  docker build --rm --target private    -t ${PRIVATE_IMAGE}:${VERSION} -t ${PRIVATE_IMAGE}:latest .
else
  echo
  echo pushing...
  docker push --all-tags $PUBLIC_IMAGE
  docker push --all-tags $PRIVATE_IMAGE
fi

