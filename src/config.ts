import nconf from 'nconf'

require('dotenv').config()

// all config values can be overriden/set using environment variables
// with __ to indicate descsending into a nested level.
// example: CALL__FORWARD__TO="my value"
const default_config = {
  // in call key press handlers
  key: {
    record: {
      announce: '5',
      no_announce: '6',
    },
  },

  // currently only a single forward number is supported by the config
  call: {
    forward: {
      to: null,
    },
  },

  tts: {
    greeting: "Hi, you've reached me on a recorded line. Press 1 if you're a human",
    voicemail: 'I am not available right now, please leave a message',
    captcha_failed: 'Goodbye robot!',
  },

  // SignalWire - required
  sw: {
    project: null as string,
    token: null as string,
  },

  // shlink.io url shortner - optional
  shortner: {
    url: null as string,
    api_key: null as string,
  },

  notification: {
    sms: {
      to: null as string,
    },
    // pushed.co
    pushed: {
      key: null as string,
      secret: null as string,
    },
  },
}

nconf.env({
  // only injest env vars that start with one of the available top level config keys
  match: new RegExp(`^${Object.keys(default_config).join('|')}`),
  separator: '__',
  lowerCase: true,
  logicalSeparator: '.',
})
nconf.defaults({ ...default_config, logicalSeparator: '.' })
nconf.required(['sw:project', 'sw:token', 'call:forward:to'])

const config = nconf.get() as typeof default_config

export { nconf }

export const showMissing = function (path: string) {
  const value = Object.assign({}, nconf.get(path))
  const missing_keys = []
  for (const key of Object.keys(value)) {
    if (value[key] === null) {
      missing_keys.push(`${path}.${key}`)
    }
    delete value[key]
  }
  return missing_keys.join(', ')
}
export default config
