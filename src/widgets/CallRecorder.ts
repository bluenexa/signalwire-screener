import _ from 'lodash'

import { BaseWidget, Context } from './BaseWidget'
import { Recorder } from '../recorder'
import { Notifier } from '../notify'

type RecorderParams = {}
export class CallRecorder extends BaseWidget {
  outputs = ['next']
  params: RecorderParams
  param_defaults = {}

  async exec(context: Context) {
    const recorder = (context.recorder = new Recorder(context.inbound_call))
    recorder.start()

    recorder.on('finished', async result => {
      this.var(context, { result })

      const notifier = new Notifier(context.client, {
        sms: { from: context.inbound_call.to },
      })
      await notifier.send(`Call recorded (${context.inbound_call.from}) ${result.duration}s ${result.url}`, {
        channels: ['sms', 'push'],
      })
    })

    // TODO: allow DTMF input to stop the recording

    return { output: 'next' }
  }
}
