import _ from 'lodash'
import { ConnectResult } from '@signalwire/node/dist/common/src/relay/calling/results'

import { shortenUrl } from '../util'
import { BaseWidget } from './BaseWidget'
import { Notifier } from '../notify'

import config from '../config'

type ConnectToParams = { to: string; timeout?: number; retries?: number }
export class ConnectTo extends BaseWidget {
  outputs = ['completed', 'failed']
  params: ConnectToParams
  param_defaults = { timeout: 30, retries: 1 }

  constructor(name: string, params: ConnectToParams) {
    super(name, params)

    if (!this.params.to) {
      throw new Error('to param required')
    }
  }

  async attemptForward(context, params): Promise<ConnectResult> {
    let retries = this.params.retries
    let connectResult: ConnectResult = null

    while (retries-- > 0) {
      // connect to forwarded call
      connectResult = await context.inbound_call.connect(params)
      this.log(context, 'connectResult', connectResult)

      if (connectResult.successful) {
        break
      } else {
        console.log('retrying call...', 'retries left:', retries)
      }
    }

    return connectResult
  }

  async exec(context) {
    const params = { type: 'phone', to: this.params.to, timeout: this.params.timeout }
    // connectResult.call is the remote leg connected with yours.
    const { successful, call } = await this.attemptForward(context, params)

    if (!successful) {
      // timeout reached
      return { output: 'failed' }
    }

    const widget = this

    const record = {
      action: null,
      active: false,
      start: async function start(call) {
        const params = {
          stereo: true,
          end_silence_timeout: 0,
          initial_timeout: 0,
          direction: 'both',
        }
        this.active = true
        const recordAction = await call.recordAsync(params)
        this.action = recordAction
        // return recordAction
      },
      stop: async function stop() {
        this.active = false
        const stopResult = await this.action.stop()

        // get the recorded file
        const recordResult = this.action.result
        if (recordResult.url) {
          const result = _.pick(recordResult, 'url', 'duration', 'size', 'state')
          result.url = await shortenUrl(result.url)
          const state = (result.state = recordResult.component.state)

          const notifier = new Notifier(context.client, {
            sms: { from: context.inbound_call.to },
          })
          await notifier.send(`Recording (${context.inbound_call.from}) ${result.duration}s ${result.url}`, {
            channels: ['sms', 'push'],
          })
        }
        widget.log(context, 'recording result', recordResult)
      },
      toggle: async function toggle(call) {
        if (this.active) {
          return this.stop()
        } else {
          return this.start(call)
        }
      },
    }

    // start a DTMF detector and handle key presses (only on the forwarded side)
    call.on('detect.update', async (call, params) => {
      try {
        this.log(context, 'Detector event', params)

        const record_keys = config.key.record

        const digit = params.detect.params.event
        // const playResult = await call.playTTS({ text: 'You entered ' + digit, gender: 'male' })

        // 4: start/stop recording, 5: start/stop recording and announce to caller
        if (digit == record_keys.announce || digit == record_keys.no_announce) {
          if (!record.active) {
            await record.start(context.inbound_call)
          } else {
            // set the flag here, but actually stop the recording after the announcments
            record.active = false
          }

          call.playTTS({ text: record.active ? 'recording started' : 'recording stopped' })
          if (digit == record_keys.announce) {
            await context.inbound_call.playTTS({
              text: record.active ? 'This call is now being recorded' : 'This call is no longer being recorded',
            })
          }

          // stop recording after the announcment has been made
          if (!record.active) {
            record.stop()
          }
        }
      } catch (e) {
        this.log(context, 'Detect error', e)
      }
    })

    const detectAction = await call.detectDigitAsync({ timeout: Number.MAX_SAFE_INTEGER })

    const success = await call.waitForEnded()
    if (record.active) {
      record.stop()
    }
    this.log(context, 'call ended, success:', success)
    return { output: 'completed' }
  }
}
