import util from 'util'

import _ from 'lodash'

import { shortenUrl } from '../util'
import { BaseWidget } from './BaseWidget'

type VoiceMailParams = { message: string; silence_timeout: number }
export class VoiceMail extends BaseWidget {
  outputs = ['recording_complete', 'no_audio']
  param_defaults = { silence_timeout: 5, max_length: 300, message: 'Please leave a message' }
  params: VoiceMailParams

  async exec(context) {
    const call = context.inbound_call

    const text = this.template(context, this.params.message)
    await call.playTTS({ text })

    const params = {
      beep: true,
      stereo: false,
      direction: 'speak', // record what the caller speaks
      initial_timeout: 5,
      end_silence_timeout: this.params.silence_timeout,
    }
    const recordResult = await call.record(params)

    // console.log(util.inspect(recordResult))
    // if(recordResult.successful){
    // console.log(util.inspect(recordResult.event))
    // }

    // const { url: longUrl, duration, size } = recordResult
    const result = _.pick(recordResult, 'url', 'duration', 'size', 'state')
    result.url = await shortenUrl(result.url)
    const state = (result.state = recordResult.component.state)
    this.var(context, result)

    switch (state) {
      case 'finished':
        return { output: 'recording_complete', value: result }
        break
      case 'no_input':
      case 'failed':
        // either there was no input, the caller hung up, or the initial_timeout happened, so end the call
        await call.hangup()
        return { output: 'no_audio', value: result }
        break
      default:
        return { output: 'error', value: recordResult.component }
    }
  }
}
