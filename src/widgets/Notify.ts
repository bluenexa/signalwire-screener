import { BaseWidget, Context } from './BaseWidget'
import { Notifier, NotifierParams } from '../notify'

type NotifyParams = { message: string; to?: string }
export class Notify extends BaseWidget {
  outputs = ['sent']
  params: NotifyParams

  constructor(name: string, params: NotifyParams) {
    super(name, params)

    if (!this.params.message) {
      throw new Error('SMS message param required')
    }
  }

  async exec(context: Context) {
    const message = this.template(context, this.params.message)

    const notifier = new Notifier(context.client, {
      sms: { from: context.inbound_call.to, to: this.params.to },
    })
    await notifier.send(message, { channels: ['sms', 'push'] })

    return { output: 'sent' }
  }
}
