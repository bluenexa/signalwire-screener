import { BaseWidget } from './BaseWidget'

type TTSParams = { message: string }
export class TTS extends BaseWidget {
  outputs = ['next']
  param_defaults = {}
  params: TTSParams

  constructor(name: string, params: TTSParams) {
    super(name, params)

    if (!this.params.message) {
      throw new Error('TTS Widget: message parameter required')
    }
  }

  async exec(context) {
    await super.exec(context)

    const call = context.inbound_call

    const text = this.template(context, this.params.message)
    console.log('TTS', text)

    await call.playTTS({ text })
    return { output: 'next' }
  }
}
