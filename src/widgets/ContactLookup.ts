import { BaseWidget, Context } from './BaseWidget'

// created with:
// contacts.json is from google people API connections.list
// https://developers.google.com/people/api/rest/v1/people.connections/list
// pageSize: 1000
// resourceName: people/me
// personFields: names,phoneNumbers,metadata,memberships,organizations
// cat contacts_raw.json | jq '.connections[] | {name: .names[]?.displayName, phone:.phoneNumbers[]?.canonicalForm, groups: [.memberships[]?.contactGroupMembership.contactGroupId]} | select(.phone!=null) | {(.phone): {name: .name, groups: .groups}}' | jq -s add > contacts.json
let default_contacts
try {
  default_contacts = require('../../assets/contacts.json')
} catch (e) {
  console.warn(`Could not load default contact list: ${e.code}`)
}

export type ContactEntry = { name: string; groups: string[]; found?: boolean }
export type ContactList = { [phone: string]: ContactEntry }

// TODO: inthe future, this will be a live call to get the latest contacts
async function lookup(contacts: ContactList, phone): Promise<ContactEntry> {
  const entry = contacts?.[phone] || { name: null, groups: null }

  const response = { found: !!entry.name, ...entry }
  return response
}

interface ContactLookupParams {
  contacts?: ContactList
  override?: boolean // set to true to allow all phone calls through, defaults to false
}
export class ContactLookup extends BaseWidget {
  outputs = ['found', 'not_found']
  params: ContactLookupParams

  constructor(name: string, params?: ContactLookupParams) {
    super(name, params)

    if (!this.params.contacts) {
      this.params.contacts = default_contacts
      // throw new Error(`${this.constructor.name}(${this.name}) contacts param required`)
    }
  }

  async exec(context: Context) {
    const caller_phone = context.inbound_call.from

    let response = await lookup(this.params.contacts, caller_phone)

    if (!response.found && this.params.override == true) {
      response = {
        found: true,
        name: 'OVERRIDE',
        groups: [],
      }
    }

    this.var(context, response)

    if (response.found) {
      return { output: 'found', value: response }
    } else {
      return { output: 'not_found', value: response }
    }
  }
}
