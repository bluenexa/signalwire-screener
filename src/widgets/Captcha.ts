import { BaseWidget } from './BaseWidget'

const events = ['pass', 'fail'] as const
type CaptchaParams = { timeout: number; message: string }
export class Captcha extends BaseWidget {
  outputs = events as any
  param_defaults = { message: '', timeout: 30 }
  params: CaptchaParams

  on(output_name: typeof events[number], dest_widget: BaseWidget, reverse_add = true) {
    return super.on(output_name, dest_widget, reverse_add)
  }

  async exec(context) {
    await super.exec(context)

    console.log(this.params)

    const call = context.inbound_call

    const params = {
      type: 'digits',
      digits_max: 1,
      digits_timeout: this.params.timeout,
      text: this.params.message,
      gender: 'male',
    }

    const promptResult = await call.promptTTS(params)

    // console.log(promptResult)

    if (!promptResult.successful) {
      // false on timeout
      return { output: 'fail' }
    }

    const type = promptResult.type // digit
    const digits = promptResult.result // digits entered by the user

    console.log('caller entered', digits)

    this.var(context, { digits })

    return { output: 'pass', value: digits }
  }
}
