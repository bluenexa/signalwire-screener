import util from 'util'

import _ from 'lodash'
import Mustache from 'mustache'
import Call from '@signalwire/node/dist/common/src/relay/calling/Call'
import { realpath } from 'fs'
import { Recorder } from '../recorder'

class Connection {
  src: BaseWidget
  dest: BaseWidget
  output: string

  constructor(src, dest, output) {
    this.src = src
    this.dest = dest
    this.output = output
  }

  toString() {
    return `${this.src.constructor.name}.${this.src.name}[${this.output}] ==> ${this.dest.constructor.name}.${this.dest.name}`
  }
}

type ExecResult = {
  output: string
  value?: any
}

export class Context {
  client
  inbound_call: Call
  recorder: Recorder

  vars = {
    widgets: {},
  }

  var(key: string, value?: any) {
    _.set(this.vars, key, value)
    return this
  }
}

export class BaseWidget {
  // list of output names
  outputs = []
  name: string
  param_defaults = {}

  params: {}

  // private fields
  #input_connections = []
  #output_connections = {}

  constructor(name: string, params = {}) {
    this.name = name
    this.params = params
  }

  set_input(src_widget, widget_output_name, reverse_add = true) {
    const connection = new Connection(src_widget, this, widget_output_name)

    this.#input_connections = this.#input_connections || []
    this.#input_connections.push(connection)

    // console.log(connection.toString())

    if (reverse_add) {
      src_widget.set_output(widget_output_name, this, false)
    }
  }

  /**
   * output_name can be either a single name or an array. Can be '*' for all supported events.
   */
  on(output_name: string, dest_widget: BaseWidget, reverse_add = true) {
    let output_names = (() => {
      if (output_name === '*') return [...this.outputs]
      return Array.isArray(output_name) ? output_name : [output_name]
    })()

    for (const output of output_names) {
      const connection = new Connection(this, dest_widget, output)

      if (!this.outputs.includes(output)) {
        throw new Error(
          `Add Output: unknown output name '${output}' for Widget '${this.name}'. Options are: ${this.outputs.join(
            ', '
          )}`
        )
      }

      this.#output_connections[output] = connection

      console.log(connection.toString())

      if (reverse_add) {
        dest_widget.set_input(this, output, false)
      }
    }

    return this
  }

  get input_connections() {
    return this.#input_connections
  }

  get output_connections() {
    return this.#output_connections
  }

  async output(context: Context, output_name: string, value: any) {
    if (output_name == 'error') {
      console.error('Widget exec error, stopping execution chain:', value)
      return
    }

    if (!this.outputs.includes(output_name)) {
      throw new Error(
        `Output: unknown output name '${output_name}' for Widget '${this.name}'. Options are: ${this.outputs.join(
          ', '
        )}`
      )
    }

    const connection = this.#output_connections[output_name]

    // automatically set a result var value
    this.var(context, 'result', value)

    console.log(`TRACE: ${this.constructor.name}: ${this.name}: ${output_name}`, value)
    console.log(`TRACE: VARS:`, context.vars)

    if (connection) {
      console.log('passing execution to:', connection.dest.name)
      await connection.dest._exec(context)
    }
  }

  template(context: Context, string = ''): string {
    return Mustache.render(string, context.vars)
  }

  // automatically sets a key/value var in the context's var's 'widgets' namespace
  var(context: Context, key: string | object, value?: any) {
    if (typeof key == 'string') {
      context.var(`widgets.${this.name}.${key}`, value)
    } else if (key !== null && typeof key != 'undefined') {
      // 'key' is an object
      for (const [k, v] of Object.entries(key)) {
        context.var(`widgets.${this.name}.${k}`, v)
      }
    }

    return this
  }

  async _exec(context: Context) {
    this.params = _.defaults(this.params, this.param_defaults)

    try {
      this.log(context, 'executing widget', `call state: ${context.inbound_call.state}`)

      let result = await this.exec(context)
      console.log('exec result', result)

      if (!result) {
        result = { output: 'default' }
      }

      await this.output(context, result.output, result.value)
    } catch (e) {
      console.error(e)
      await this.output(context, 'error', { error: e.message })
    }
  }

  // do the magic here and pass the outputs to the right output connections
  async exec(context: Context): Promise<ExecResult> {
    return
  }

  // util methods
  log(context, label, value) {
    const inspected = util.inspect(value || '', false, 5)
    console.log(`[${this.constructor.name}: ${this.name} (${context.inbound_call.id})]: ${label}`, value)
  }
}
