import { RelayClient } from '@signalwire/node'
import got from 'got'

import config, { showMissing } from './config'

if (!config.notification.sms.to) {
  console.warn(
    'Warning: SMS notification channel disabled: required configuration missing:',
    showMissing('notification.sms')
  )
}

const { key: PUSHED_APP_KEY, secret: PUSHED_APP_SECRET } = config.notification.pushed

if (!(PUSHED_APP_KEY && PUSHED_APP_SECRET)) {
  console.warn(
    'Warning: pushed.co notification channel disabled: required configuration missing:',
    showMissing('notification.pushed')
  )
}

export type NotifierChannel = 'sms' | 'push'
export type NotifierParams = { sms: { from: string; to?: string } }
export class Notifier {
  client: RelayClient
  params: NotifierParams

  constructor(client: RelayClient, params: NotifierParams) {
    this.client = client
    this.params = params

    params.sms.to = params.sms.to || config.notification.sms.to
  }

  async send(message, options: { channels: NotifierChannel[]; url?: string }) {
    const channels: NotifierChannel[] = options.channels || ['sms', 'push']

    if (channels.includes('sms')) {
      await this._send_sms(message, options.url)
    }

    if (channels.includes('push')) {
      await this._send_push(message, options.url)
    }
  }

  private async _send_sms(message: string, url?: string) {
    try {
      if (!this.params.sms.to) {
        return
      }

      const sendResult = await this.client.messaging.send({
        context: 'notification',
        from: this.params.sms.from,
        to: this.params.sms.to,
        body: message,
      })

      if (sendResult.successful) {
        console.log('sms succeeded', sendResult.messageId)
      } else {
        console.error('sms failed', sendResult.errors)
      }
    } catch (e) {
      console.error('sms failed', e)
    }
  }

  private async _send_push(message: string, url?: string) {
    if (!(PUSHED_APP_KEY && PUSHED_APP_SECRET)) {
      return
    }

    // curl -X POST -s   --form-string "app_key=$PUSHED_APP_KEY"   --form-string "app_secret=$PUSHED_APP_SECRET"   --form-string "target_type=app"   --form-string "content=api send 1"   https://api.pushed.co/1/push;

    const form = {
      app_key: PUSHED_APP_KEY,
      app_secret: PUSHED_APP_SECRET,
      target_type: 'app',
      content: message,
    }

    // extract the URL and send it differently
    const urls = message.match(/\bhttps?:\/\/\S+/g)
    if (urls && urls[0]) {
      form['content_type'] = 'url'
      form['content_extra'] = urls[0]
    }

    try {
      const response = await got
        .post('https://api.pushed.co/1/push', {
          form,
        })
        .json()

      console.log('push succeeded', response)
    } catch (e) {
      console.error('push failed', e)
    }
  }
}
