import _ from 'lodash'

import { RelayConsumer } from '@signalwire/node'
import Call from '@signalwire/node/dist/common/src/relay/calling/Call'

import config from './config'
import { sleep } from './util'
import * as widgets from './widgets'
import { BaseWidget } from './widgets'

// helper function to make it easier to chain notifications below
function notify(name: string, message: string, next?: BaseWidget) {
  const msg = new widgets.Notify(name, { message })
  if (next) {
    msg.on('*', next)
  }
  return msg
}

/// execution graph ////
const captcha = new widgets.Captcha('captcha', { message: config.tts.greeting })
const vm = new widgets.VoiceMail('voicemail', { message: config.tts.voicemail })
vm.on(
  '*',
  notify(
    'vm_notify',
    'VM {{ widgets.voicemail.state }} ({{ widgets.voicemail.duration }}s) {{{ widgets.voicemail.url }}}'
  )
)

const forward = new widgets.ConnectTo('forward', { to: config.call.forward.to })
// forward.on('completed', new widgets.TTS('forward_completed', {message: 'foward completed'}))
forward.on('failed', vm)

const contacts_lookup = new widgets.ContactLookup('contact_lookup')
const incoming_notification_message = 'call from {{ call.from }} ({{ widgets.contact_lookup.result.name }})'
contacts_lookup.on('found', notify('incoming_found', incoming_notification_message, forward))
contacts_lookup.on(
  'not_found',
  new widgets.CallRecorder('recorder').on('next', notify('incoming_not_found', incoming_notification_message, captcha))
)

// captcha.on('pass', new widgets.TTS('passed', {message: 'hello human, you entered {{ widgets.captcha.digits }}!'}))
captcha.on('pass', notify('captcha_passed', 'captcha passed with {{ widgets.captcha.digits }}', forward))
captcha.on(
  'fail',
  notify('captcha_failed', 'captcha failed', new widgets.TTS('failed', { message: config.tts.captcha_failed }))
)

const graph = contacts_lookup

const consumer = new RelayConsumer({
  project: config.sw.project,
  token: config.sw.token,
  contexts: ['screener', 'notification'],
  ready: async consumer => {
    console.log('ready')
  },
  onIncomingCall: async (call: Call) => {
    try {
      // sleep for 3 seconds so we don't pick up too fast
      console.log('incoming call, sleeping')
      await sleep(3000)

      console.log('answering...')
      const { successful } = await call.answer()
      if (!successful) {
        return
      }

      const context = new widgets.Context()

      // client is protected on RelayConsumer
      context.client = (consumer as any).client
      context.inbound_call = call

      // make the call available to templates
      context.var('call', call)

      const events = [
        'stateChange',
        'created',
        'ringing',
        'answered',
        'ending',
        'ended',
        'connect.stateChange',
        'connect.connecting',
        'connect.connected',
        'connect.failed',
        'connect.disconnected',
      ]
      events.forEach(event => {
        call.on(event, async (_call, params) => {
          console.log(`call event [${_call.state}]`, event, params || '')
        })
      })

      await graph._exec(context)

      // console.log(util.inspect(context, {depth: 5}))
      console.log('hanging up')
      await call.hangup()
    } catch (e) {
      console.error(e)
      await call.hangup()
    }
  },
})

consumer.run()
