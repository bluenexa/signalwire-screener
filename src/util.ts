import util from 'util'
import got from 'got'

import config, { showMissing } from './config'

const { url: SHORTNER_URL, api_key: SHORTNER_API_KEY } = config.shortner

const enabled = SHORTNER_URL && SHORTNER_API_KEY

if (!enabled) {
  console.warn('Warning: URL shortning disabled: required configuration missing:', showMissing('shortner'))
}

export async function shortenUrl(url: string): Promise<string> {
  try {
    if (!url || !enabled) {
      return url
    }

    // const pkg = require('../package.json');
    const body = await got
      .post(`${SHORTNER_URL}/rest/v2/short-urls`, {
        headers: {
          'X-Api-Key': SHORTNER_API_KEY,
          // 'user-agent': `${pkg.name}/${pkg.version}`,
          'user-agent': `signalwire-screener`,
        },
        json: {
          longUrl: url,
          tags: ['recording', 'vm'],
          findIfExists: true,
        },
      })
      .json()

    // example response body:
    // {
    //   "shortCode": "zP5Ht",
    //   "shortUrl": "https://short.url/zP5Ht",
    //   "longUrl": "http://google.com/bah",
    //   "dateCreated": "2020-12-16T06:59:31+00:00",
    //   "visitsCount": 0,
    //   "tags": [
    //     "recording",
    //     "vm"
    //   ],
    //   "meta": {
    //     "validSince": null,
    //     "validUntil": null,
    //     "maxVisits": null
    //   },
    //   "domain": null
    // }

    return (body as any).shortUrl
  } catch (e) {
    // on error, log it and return the orignal URL
    console.error(e)
    return url
  }
}

export const sleep = util.promisify((ms, cb) => setTimeout(cb, ms))
