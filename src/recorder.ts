import { EventEmitter } from 'events'

import _ from 'lodash'

import { RecordAction } from '@signalwire/node/dist/common/src/relay/calling/actions'
import Call from '@signalwire/node/dist/common/src/relay/calling/Call'

import { shortenUrl } from './util'

export class Recorder extends EventEmitter {
  call: Call
  action: RecordAction
  active = false

  constructor(call: Call) {
    super()
    this.call = call
  }

  async start() {
    if (this.active) {
      return
    }

    this.active = true

    const params = {
      stereo: true,
      end_silence_timeout: 0,
      initial_timeout: 0,
      direction: 'both',
    }

    this.action = await this.call.recordAsync(params)

    const finished_handler = async (call, params) => {
      // make sure we only process the recording started by this recorder
      if (this.action.controlId != params.control_id) {
        return
      }

      // call.off('record.finished', finished_handler)

      // console.log('call recorder finished', call)

      // get the recorded file
      const recordResult = this.action.result
      if (recordResult && recordResult.successful) {
        const result = _.pick(recordResult, 'url', 'duration', 'size', 'state') as any
        result.url = await shortenUrl(result.url)
        const state = (result.state = recordResult.component.state)

        this.emit('finished', result)
      }
    }

    // start a listener for the finished event so that we capture and send the recording url
    this.call.on('record.finished', finished_handler)
  }

  async stop() {
    if (!this.active) {
      return
    }
    this.active = false
    await this.action.stop()
  }

  async toggle() {
    if (this.active) {
      return this.stop()
    } else {
      return this.start()
    }
  }
}
